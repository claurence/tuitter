import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import * as icons from "@fortawesome/free-solid-svg-icons"

import {addPublication, getPublication, likePublication} from "../src/Services/publicationService";


const Publication = () => {

    const [publications, setpublication] = React.useState();
    const [content, setcontent] =React.useState();
    const [refresh, setrefresh] = React.useState(true);

    const publish = (e) => {
        e.preventDefault();

        if (content){
            addPublication(content)
        }
    }

    React.useEffect(() => {

        const fecthPublication = async () => {
            const fetchData = await getPublication();
            setpublication(fetchData);
        }
        if (refresh) {
            fecthPublication();
            setrefresh(false);
        }

    }, [refresh])


    return(
        <div className={"pt-5 text-center "}>
            <button className={"btn btn-dark m-5 d-flex justify-content-center"} onClick={()=> setrefresh(true)}><FontAwesomeIcon className={"mr-3"} icon={icons.faSyncAlt}/> Refresh</button>

            <div className={"row Header-publi text-center m-5"}>
                <textarea className={"col-12"} id={"publi"} name={"publi"} rows={"5"} cols={"50"} placeholder={"contenu de votre publication..."} onChange={(e)=> setcontent(e.target.value)} ></textarea>
                <button className={"btn btn-info d-flex justify-content-center"} label="publier" onClick={(e)=> publish(e)}>Publier</button>
            </div>
            <div className={"row"}>
                {publications ?
                    publications.map(publication => (
                    <div className={ 'publi-content mx-auto mb-3' }>
                        <p className={'text-center text-content'}>{publication.content}</p>
                        <div className={"row content-like"}>
                            <p className={"col-6 mt-2"}>Nombre de like: {publication.like}</p>
                            <button onClick={() => likePublication(publication.id)} className={"btn text-danger mb-1 col-1"} target={"blank"} margin={'small'}><FontAwesomeIcon icon={icons.faHeart}/></button>
                        </div>
                    </div>
                    ))
                    :
                    <p>Il n'y a pas de publication pour le moment ...</p>
                }
            </div>

        </div>
    )
}

export default Publication;
