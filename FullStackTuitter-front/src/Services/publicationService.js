import axios from "axios";

import fire from '../firebase';

const url = 'http://localhost:3003/publication'

const createToken = async () => {
    const user = fire.auth().currentUser;
    const token = user && (await user.getIdToken());
    const payloadHeader = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
        }
    };

    return payloadHeader;
}

export const addPublication = async (content) => {
    const header = await createToken();
    const user = fire.auth().currentUser.email;
    const like = 0;
    const payload ={
        content,
        user,
        like
    }
    try {
        const res = await axios.post(url, payload, header);
        return res.data;
    }catch (e) {
        console.error(e);
    }

}

export const getPublication = async () => {
    const header = await createToken();
    try {
        const res = await axios.get(url, header)
        return res.data;
    } catch (e) {
        console.error(e);
    }
}

export const likePublication = async (idPost) => {
    const header = await createToken();
    console.log(idPost);
    const payload ={
        idPost
    }
    try {
        const res = await axios.put(url + "/like", payload, header);
        return res.data;
    }catch (e) {
        console.error(e);
    }
}
