import React from "react";


import fire from "../firebase"

const Login = () => {
    const [email, setEmail] = React.useState();
    const [password, setPassword] = React.useState();
    const [alert, setalert] =React.useState();

    const handleSubmit = (e) => {
        e.preventDefault();
        fire.auth().signInWithEmailAndPassword(email, password)
            .catch((error) => {
                setalert('Incorrect username or password');
            });
    }

    return(
        <div className={'containerApp'}>
            <div className={"titleApp"}>
                <div className={"textTittle"}>
                    <h1 className={"mt-3"}>Welcome to mini project Full stack use this page to login</h1>
                </div>
            </div>
            <div className={"containerLogin"}>
                <h2 className={" text-uppercase mb-4"}>Login</h2>
                <div className="mb-3">
                    <input type="text" className="form-control" placeholder="Email" onChange={(e => setEmail(e.target.value))}/>
                </div>
                <div className="input-group mb-3">
                    <input type="password" placeholder={"Password"} className="form-control" onChange={(e => setPassword(e.target.value))}/>
            </div>

            {alert && (
                <p className={"text-danger"}>{alert}</p>
            )}

            <button className={"btn btn-dark float-right"} label="ok" onClick={(e)=> handleSubmit(e)}>LOGIN</button>
        </div>
        </div>
    )
}

export default Login
