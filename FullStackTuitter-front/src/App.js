import React from "react";

import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';


import fire from "./firebase";
import Login from "./Services/Login";
import Publication from "./publication";


function App() {

  const [isLoggedIn, setIsLoggedIn] = React.useState(false);

  fire.auth().onAuthStateChanged((user) => {
    return user ? setIsLoggedIn(true) : setIsLoggedIn(false);
  });



    return (
    <div className="App">

      <Router>

        {!isLoggedIn
            ? (
                <Route path="/" exact>
                  <Login />
                </Route>
            )
            : (
                <div>
                    <p className={"m-2"}>Connecté en tant que {fire.auth().currentUser.email}</p>
                        <button className={"btn btn-dark text-uppercase logout-btn float-right m-2"} label="sign out" onClick={()=> fire.auth().signOut()}>Se déconnecter</button>
                    <Publication />
                </div>
            )}
      </Router>

    </div>
  );
}

export default App;
