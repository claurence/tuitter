import firebase from "firebase";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyAPrt9gaWEfmSYJ7Cn2t_fm6P37RJTtEWs",
    authDomain: "fullstacktuitter.firebaseapp.com",
    projectId: "fullstacktuitter",
    storageBucket: "fullstacktuitter.appspot.com",
    messagingSenderId: "47553883683",
    appId: "1:47553883683:web:c6582a539d8a99457c8b96",
    measurementId: "G-1EE69LCQS2"
};

try {
    firebase.initializeApp(firebaseConfig);
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error', err.stack);
    }
}
const fire = firebase;
export default fire;
