const publicationRouter = require('express').Router();
const Publication = require('../models/publication');

publicationRouter.get('/', async (req, res) => {

    const auth = req.currentUser;
    if (auth){
        const publications = await Publication.find({});
        return res.json(publications.map((publication => publication.toJSON())));
    }
    return res.status(403).send('Not authorized');
});

publicationRouter.post('/', (req, res)=> {
    const auth = req.currentUser;
    if (auth){
        const publication = new Publication(req.body)
        const savedPublication = publication.save()
        return res.status(201).json(savedPublication);
    }
    return res.status(403).send('Not authorized')

});

publicationRouter.put('/like', async(req, res)=> {
    const auth = req.currentUser;
    if (auth){
        const savedPublication = await Publication.findOne({ _id: req.body.idPost });
        const newLike = savedPublication.like + 1;

        const result = await Publication.updateOne(
            { "_id": savedPublication.id}, // Filter
            {$set: {"like": newLike}}, // Update
        );

        return res.status(201).json(result);
    }
    return res.status(403).send('Not authorized')

});

module.exports = publicationRouter;