const mongoose = require('mongoose');

const publicationSchema = new mongoose.Schema({
    content: String,
    user: String,
    like: Number
})

publicationSchema.set('toJSON', {
    transform : (doc, returnedObject) => {
        returnedObject.id = returnedObject._id.toString();
        delete returnedObject._id;
        delete returnedObject._v;
    }
})

module.exports = mongoose.model('Publication', publicationSchema);