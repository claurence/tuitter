const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const decodeIDToken = require('./authenticateToken');
const publicationRouter = require('./controllers/publication');

const app = express();
const server = require('http').Server(app)
const io = require('socket.io')(server)


app.use(cors());
app.use(decodeIDToken);
app.use(express.json());


mongoose.connect(
    'mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false',
    {
        useNewUrlParser: true, useUnifiedTopology: true
    }
).then(() => {
    console.log('Connected to database');
}).catch((err) => console.log('Error connecting database', err.message));


app.use('/publication', publicationRouter)

let interval;

io.on("connection", (socket) => {
    console.log("New client connected");
    if (interval) {
        clearInterval(interval);
    }
    interval = setInterval(() => getApiAndEmit(socket), 1000);
    socket.on("disconnect", () => {
        console.log("Client disconnected");
        clearInterval(interval);
    });
});

const getApiAndEmit = socket => {
    const response = new Date();
    // Emitting a new message. Will be consumed by the client
    socket.emit("FromAPI", response);
};

app.get('/', (req, res) => {
    res.send('FullStack Tuitter');
});

const PORT = 3003;

app.listen(PORT, () => {
    console.log(`Tuitter is running on port ${PORT}`);
});